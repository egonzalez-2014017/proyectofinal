<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
	HttpSession sesion=request.getSession();
	Object user=sesion.getAttribute("usuario");
	if(user == null){
		%>
		<jsp:forward page="/index.jsp" >
			<jsp:param name="error" value="Para Acceder al Sistema, por favor Inicie Sesi�n."/>
		</jsp:forward>
		<%
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<a href="index.jsp">Salir</a>
	<h1>Paquetes</h1>
	<a href="AgregarPaquete" method="get">Agregar Paquetes</a>
	<br>
	<br>
	<table>
		<thead>
			<tr>
				<th>NOMBRE RECEPTOR</th>
				<th>NOMBRE EMISOR</th>
				<th>FECHA ENTREGA</th>
				<th>FECHA EMISION</th>
				<th>DIRECCION EMISOR</th>
				<th>DIRECCION RECEPTOR</th>
				<th>COSTO</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${listaPaquetes}" var="paquete">
				<tr>
					<td>${paquete.getNombreReceptor()}</td>
					<td>${paquete.getNombreEmisor()}</td>
					<td>${paquete.getFechaEntrega()}</td>
					<td>${paquete.getFechaEmision()}</td>
					<td>${paquete.getDireccionEmisor()}</td>
					<td>${paquete.getDireccionReceptor()}</td>
					<td>${paquete.getCosto()}</td>
					<td><a href="EliminarPaquete?idPaquete=${paquete.getIdPaquete()}">Eliminar</a></td>
					<td><a href="EditarPaquete?idPaquete=${paquete.getIdPaquete()}">Editar</a></td>
					<td><a href="Articulos?idPaquete=${paquete.getIdPaquete()}">Articulos del paquete</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>