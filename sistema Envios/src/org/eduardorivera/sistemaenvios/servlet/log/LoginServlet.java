package org.eduardorivera.sistemaenvios.servlet.log;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.eduardorivera.sistemaenvios.bean.Usuario;
import org.eduardorivera.sistemaenvios.db.Conexion;

@WebServlet("/login")
public class LoginServlet extends HttpServlet{

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		RequestDispatcher despachador = null;
		List<Object> usuarios=Conexion.getInstancia().autenticar(req.getParameter("txtUsuario"),req.getParameter("txtPassword"));
		if(usuarios!=null && usuarios.size()>0){
			HttpSession sesion=req.getSession(true);
			sesion.setAttribute("usuario", (Usuario)usuarios.get(0));
			despachador=req.getRequestDispatcher("menu.jsp");
		}else{
			req.setAttribute("error", "verifica tus credenciales");
			despachador=req.getRequestDispatcher("index.jsp");
		}
		despachador.forward(req, res);
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		doPost(req, res);
	}

}
