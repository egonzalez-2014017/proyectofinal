package org.eduardorivera.sistemaenvios.servlet.paquetes;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eduardorivera.sistemaenvios.bean.Usuario;
import org.eduardorivera.sistemaenvios.db.Conexion;

@WebServlet("/paquetes")
public class PaquetesServlet extends HttpServlet{

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		RequestDispatcher despachador = null;
		Usuario us = (Usuario) req.getSession(true).getAttribute("usuario");
		req.setAttribute("listaPaquetes",Conexion.getInstancia().listar(
				"FROM Paquete c where c.idUsuario="+us.getIdUsuario()));
		despachador = req.getRequestDispatcher("/paquetes/paquetes.jsp");
		despachador.forward(req, res);
	}

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		doPost(req, res);
	}
}