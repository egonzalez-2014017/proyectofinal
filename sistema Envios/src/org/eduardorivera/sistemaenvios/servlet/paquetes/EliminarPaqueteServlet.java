package org.eduardorivera.sistemaenvios.servlet.paquetes;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eduardorivera.sistemaenvios.bean.Paquete;
import org.eduardorivera.sistemaenvios.db.Conexion;


@WebServlet("/EliminarPaquete")
public class EliminarPaqueteServlet extends HttpServlet{

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		RequestDispatcher despachador = null;
		Integer id = Integer.parseInt(req.getParameter("idPaquete"));
		Conexion.getInstancia().eliminar(Conexion.getInstancia().obtener(
				Paquete.class, id ));
		despachador = req.getRequestDispatcher("paquetes");
		despachador.forward(req, res);
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		doPost(req, res);
	}

}
