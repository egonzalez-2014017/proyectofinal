package org.eduardorivera.sistemaenvios.servlet.paquetes;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eduardorivera.sistemaenvios.bean.Paquete;
import org.eduardorivera.sistemaenvios.bean.Usuario;
import org.eduardorivera.sistemaenvios.db.Conexion;


@WebServlet("/AgregarPaquete")
public class AgregarPaqueteServlet extends HttpServlet{

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {
		RequestDispatcher despachador = null;
		Paquete paquete = new Paquete(
				0,
				req.getParameter("txtNombreEmisor"),
				req.getParameter("txtNombreReceptor"),
				req.getParameter("txtFechaEntrega"),
				req.getParameter("txtFechaEmision"),
				req.getParameter("txtDireccionEmisor"),
				req.getParameter("txtDireccionReceptor"),
				Double.parseDouble(req.getParameter("txtCosto")),
				(Usuario) req.getSession(true).getAttribute("usuario")
				);
		Conexion.getInstancia().agregar(paquete);
		despachador = req.getRequestDispatcher("paquetes");
		despachador.forward(req, res);
	}

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {
		RequestDispatcher despachador = null;
		despachador = req.getRequestDispatcher("/paquetes/agregarPaquete.jsp");
		despachador.forward(req, res);
	}

}