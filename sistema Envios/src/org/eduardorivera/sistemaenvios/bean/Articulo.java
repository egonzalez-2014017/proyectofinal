package org.eduardorivera.sistemaenvios.bean;

public class Articulo {

	private Integer idArticulo;
	private String nombre;
	private String descripcion;
	private Double precio;
	private TipoArticulo idTipo;
	private Paquete idPaquete;

	public Paquete getIdPaquete() {
		return idPaquete;
	}

	public void setIdPaquete(Paquete idPaquete) {
		this.idPaquete = idPaquete;
	}

	public Integer getIdArticulo() {
		return idArticulo;
	}

	public void setIdArticulo(Integer idArticulo) {
		this.idArticulo = idArticulo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Articulo() {
		// TODO Auto-generated constructor stub
	}

	public Articulo(Integer idArticulo, String nombre, String descripcion, Double precio, TipoArticulo idTipo,
			Paquete idPaquete) {
		super();
		this.idArticulo = idArticulo;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.precio = precio;
		this.idTipo = idTipo;
		this.idPaquete = idPaquete;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public TipoArticulo getIdTipo() {
		return idTipo;
	}

	public void setIdTipo(TipoArticulo idTipo) {
		this.idTipo = idTipo;
	}

}
