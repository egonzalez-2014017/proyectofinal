package org.eduardorivera.sistemaenvios.bean;


public class Paquete {

	private Integer idPaquete;
	private String nombreReceptor;
	private String nombreEmisor;
	private String fechaEntrega;
	private String fechaEmision;
	private String direccionEmisor;
	private String direccionReceptor;
	private Double costo;
	private Usuario idUsuario;

	public Integer getIdPaquete() {
		return idPaquete;
	}

	public void setIdPaquete(Integer idPaquete) {
		this.idPaquete = idPaquete;
	}

	public String getNombreReceptor() {
		return nombreReceptor;
	}

	public void setNombreReceptor(String nombreReceptor) {
		this.nombreReceptor = nombreReceptor;
	}

	public String getNombreEmisor() {
		return nombreEmisor;
	}

	public void setNombreEmisor(String nombreEmisor) {
		this.nombreEmisor = nombreEmisor;
	}

	public String getFechaEntrega() {
		return fechaEntrega;
	}

	public void setFechaEntrega(String fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}

	public String getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public String getDireccionEmisor() {
		return direccionEmisor;
	}

	public void setDireccionEmisor(String direccionEmisor) {
		this.direccionEmisor = direccionEmisor;
	}

	public String getDireccionReceptor() {
		return direccionReceptor;
	}

	public void setDireccionReceptor(String direccionReceptor) {
		this.direccionReceptor = direccionReceptor;
	}

	public Double getCosto() {
		return costo;
	}

	public void setCosto(Double costo) {
		this.costo = costo;
	}

	public Usuario getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Usuario idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Paquete(Integer idPaquete, String nombreReceptor,
			String nombreEmisor, String fechaEntrega, String fechaEmision,
			String direccionEmisor, String direccionReceptor, Double costo,
			Usuario usuario) {
		super();
		this.idPaquete = idPaquete;
		this.nombreReceptor = nombreReceptor;
		this.nombreEmisor = nombreEmisor;
		this.fechaEntrega = fechaEntrega;
		this.fechaEmision = fechaEmision;
		this.direccionEmisor = direccionEmisor;
		this.direccionReceptor = direccionReceptor;
		this.costo = costo;
		this.idUsuario = usuario;
	}

	public Paquete() {
		// TODO Auto-generated constructor stub
	}

}

