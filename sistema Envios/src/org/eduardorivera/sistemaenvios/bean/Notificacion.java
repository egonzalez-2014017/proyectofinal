package org.eduardorivera.sistemaenvios.bean;

public class Notificacion {

	private Integer idNotificacion;
	private String mensaje;
	private Usuario idEmisor;
	private Usuario idReceptor;

	public Integer getIdNotificacion() {
		return idNotificacion;
	}

	public void setIdNotificacion(Integer idNotificacion) {
		this.idNotificacion = idNotificacion;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Usuario getIdEmisor() {
		return idEmisor;
	}

	public void setIdEmisor(Usuario idEmisor) {
		this.idEmisor = idEmisor;
	}

	public Usuario getIdReceptor() {
		return idReceptor;
	}

	public void setIdReceptor(Usuario idReceptor) {
		this.idReceptor = idReceptor;
	}

	public Notificacion(Integer idNotificacion, String mensaje,
			Usuario idEmisor, Usuario idReceptor) {
		super();
		this.idNotificacion = idNotificacion;
		this.mensaje = mensaje;
		this.idEmisor = idEmisor;
		this.idReceptor = idReceptor;
	}

	public Notificacion() {
		// TODO Auto-generated constructor stub
	}

}
