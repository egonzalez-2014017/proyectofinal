package org.eduardorivera.sistemaenvios.bean;

public class TipoArticulo {

	private Integer idTipo;
	private String nombre;
	private Double precioExtra;

	public Integer getIdTipo() {
		return idTipo;
	}

	public void setIdTipo(Integer idTipo) {
		this.idTipo = idTipo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Double getPrecioExtra() {
		return precioExtra;
	}

	public void setPrecioExtra(Double precioExtra) {
		this.precioExtra = precioExtra;
	}

	public TipoArticulo(Integer idTipo, String nombre, Double precioExtra) {
		super();
		this.idTipo = idTipo;
		this.nombre = nombre;
		this.precioExtra = precioExtra;
	}

	public TipoArticulo() {
		// TODO Auto-generated constructor stub
	}

}
